# Daqling spack repos

These instructions might not work.

## Installing the packages.

## Prerequisites

Clone the repository, containg spack and custom packages.

Use *--recurse-submodules* to initialize and update the spack sub-module.

    git clone --recurse-submodules https://:@gitlab.cern.ch:8443/ep-dt-di/daq/daqling-spack-repo.git

A GCC C++17 enabled compiler is required.

For CERN CentOS 7:

    yum install http://build.openhpc.community/OpenHPC:/1.3/CentOS_7/x86_64/ohpc-release-1.3-1.el7.x86_64.rpm
    yum install gnu8-compilers-ohpc cmake
    export PATH=$PATH:/opt/ohpc/pub/compiler/gcc/8.3.0/bin

For CentOS 8:

    yum install gcc-c++ libasan libubsan cmake

For Ubuntu (Server):

    sudo apt install build-essential cmake

## Run the install script

The install script should take care of the rest:

    cd daqling-spack-repo/
    ./Install.sh

## Use the created environment

The install script creates and installs packages in the `daqling` spack-environment.

To use this environment with daqling, source the `cmake/setup.sh` of the `develop` daqling branch: 
https://gitlab.cern.ch/ep-dt-di/daq/daqling/-/tree/develop

Note: make sure that `setup.sh` points to the full path of your daqling-spack-repo location.

## Build daqling

Commands to build daqling. (When standing outside daqling repos)

    cd daqling
    source cmake/setup.sh
    mkdir build
    cd build
    cmake ../
    make -j


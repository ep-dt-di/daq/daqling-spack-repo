if [ $# -gt 1 ];
then
  echo "Invalid number of arguments: $#"
  return 1
fi
ARCH=" "
if [ $# -eq 1 ]
then
  ARCH="target=$1"
fi
BASEDIR="$(dirname "$(realpath "$BASH_SOURCE")" )"

source $BASEDIR/spack/share/spack/setup-env.sh
echo "spack install nlohmann-json ${ARCH} cppzmq@4.3.0 ${ARCH} xmlrpc-c@1.51.06 ${ARCH} cpr ${ARCH} ers ${ARCH} folly-incl ${ARCH} moody-incl ${ARCH}"
echo "Creating and activating 'daqling' env."
spack env create daqling
spack env activate daqling

echo "Adding daqling repository to spack."
spack repo add "$BASEDIR"/daqling-repo

spack compiler find
spack compilers

echo "Beginning installation."
spack add nlohmann-json ${ARCH} cppzmq@4.3.0 ${ARCH} xmlrpc-c@1.51.06 ${ARCH} curl ${ARCH} ers ${ARCH} folly-incl ${ARCH} moody-incl ${ARCH}
spack install -j 4
spack load curl
spack add cpr
spack install -j 4

if [ $? -eq 0 ]
then
  echo "The dependencies have been succesfully installed"
  exit 0
else
  echo "The installation failed" >&2
  exit 1
fi

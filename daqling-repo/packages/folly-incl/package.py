# Copyright 2013-2020 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *


class FollyIncl(Package):
    """Folly contains a variety of core library components used extensively at Facebook."""

    homepage = "https://github.com/facebook/folly"
    url      = "https://github.com/facebook/folly/releases/download/v2021.03.22.00/folly-v2021.03.22.00.tar.gz"

    maintainers = ['egamberini']

    version('2021.03.22.00', sha256='1e1ee0fa1dd61763f4ea87bad76362f39cdbc0ecb599893693b3013e7d43ed87')

    # depends_on('boost@1.70.0')

    def install(self, spec, prefix):
      folly_source = 'folly'
      folly_include = join_path(prefix.include, folly_source)
      mkdirp(folly_include)
      install_tree(folly_source, folly_include)


# Copyright 2013-2020 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *


class MoodyIncl(CMakePackage):
    """Moody Camel ReaderWriterQueue provides a fast lock-free producer consumer queue"""

    homepage = "https://github.com/cameron314/readerwriterqueue"
    url      = "https://github.com/cameron314/readerwriterqueue/archive/refs/tags/v1.0.4.tar.gz"

    maintainers = ['egamberini']

    version('1.0.4','d5812f2ff435f6912b0285de876063ac')

    # depends_on('boost@1.70.0')

    def install(self, spec, prefix):
      moody_source = '.'
      moody_include = join_path(prefix.include,'moody')
      mkdirp(moody_include)
      install_tree(moody_source, moody_include)


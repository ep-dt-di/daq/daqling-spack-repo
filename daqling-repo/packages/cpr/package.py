# Copyright 2013-2020 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
#     spack install cpr
#
# You can edit this file again by typing:
#
#     spack edit cpr
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

from spack import *

class Cpr(CMakePackage):
    """C++ Requests is a simple wrapper around libcurl inspired by the excellent Python Requests project."""

    homepage = "https://whoshuu.github.io/cpr"
    url      = "https://github.com/whoshuu/cpr/archive/1.6.2.tar.gz"
    maintainers = ['com8', 'tstack']

    version('1.6.2', sha256='c45f9c55797380c6ba44060f0c73713fbd7989eeb1147aedb8723aa14f3afaa3')

    depends_on('curl')

    def cmake_args(self):
        args = [
            '-DCPR_FORCE_USE_SYSTEM_CURL=ON',
            '-DCPR_BUILD_TESTS=OFF'
        ]
        return args

    def install(self, spec, prefix):
        mkdirp(prefix.include)
        
        install_tree('./include', prefix.include)

        mkdirp(prefix.lib)
        install_tree(join_path(self.build_directory, 'lib'), prefix.lib)

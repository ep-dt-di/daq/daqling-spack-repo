#Copyright 2013-2020 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)


from spack import *


class Ers(CMakePackage):
    """Ers Provides chained events, and a logging system capable of reporting errors as well as regular strings."""

    homepage = "https://gitlab.cern.ch/ep-dt-di/daq/ers"
    url      = "https://gitlab.cern.ch/ep-dt-di/daq/ers/-/archive/master/ers-master.tar.gz"


    version('1.2.3', '9740f09b524dd1f347b7ba9abf27f769')

    depends_on('boost +regex cxxstd=17')

    def cmake_args(self):
        args=[
            '-DCMAKE_INSTALL_PREFIX=./lib',
        ]

        return args
    

    def install(self, spec, prefix):
        ers_source = 'ers'
        ers_include = join_path(prefix.include, ers_source)
        mkdirp(ers_include)
        install_tree(ers_source, ers_include)

        mkdirp(prefix.lib)
        install_tree(join_path(self.build_directory, 'src'), prefix.lib)
